INSERT INTO cliente VALUES (1, '11111122222', 'Maria Fernanda Nobrega', 'R. Dom João V, 120 – Miramar','f', '1990-04-12');
INSERT INTO cliente VALUES (2, '96452997623', 'Natalia Campos', 'R. Padre Ibiapina, 56 – Cabo Branco','o', '1998-07-02');
INSERT INTO cliente VALUES (3, '75432334512', 'Jorge Alencar', 'R. Boa Esperança, 89 – Miramar','m', '1993-01-23');
INSERT INTO cliente VALUES (4, '97463566541', 'Gabriel Nascimento', 'R. Dom João V, 310 – Miramar','o', '1988-04-12');
INSERT INTO cliente VALUES (5, '74635928374', 'Leticia Alencar', 'R. da Paz, 21 – Bancários','f', '1988-09-01');

SELECT * FROM cliente;

INSERT INTO carro VALUES (1,'spin','chevrolet','ABC-1342');
INSERT INTO carro VALUES (2,'siena','fiat','PPG-7763');
INSERT INTO carro VALUES (3,'soul','kia','AAA-5522');
INSERT INTO carro VALUES (4,'sandero','renault','NAU-8332');

SELECT * FROM carro;

INSERT INTO funcionario VALUES (1,'314264','Paulo Andrade');
INSERT INTO funcionario VALUES (2,'089785','Vitoria Velo');

SELECT * FROM funcionario;

SELECT C.nome, F.nome 
FROM aluguel A
INNER JOIN cliente C
on (C.codigo = A.cod_cliente)
INNER JOIN funcionario F
ON (F.codigo = A.cod_funcionario) WHERE F.nome = 'Vitoria Veloso';

SELECT nome, datanascimento FROM cliente
WHERE datanascimento BETWEEN '1988-01-01' AND '1988-12-31';

SELECT nome, datanascimento FROM cliente
WHERE datanascimento BETWEEN '1990-01-01' AND '1999-12-31';

SELECT * FROM cliente
WHERE nome LIKE '%Alencar%';

SELECT sexo, COUNT(*) AS quantidade FROM cliente
GROUP BY sexo 
HAVING sexo ='f';
