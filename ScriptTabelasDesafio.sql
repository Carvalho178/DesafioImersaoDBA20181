CREATE TABLE cliente (
	codigo NUMERIC PRIMARY KEY,
	cpf CHAR(11) NOT NULL UNIQUE,
	nome VARCHAR(50) NOT NULL,
	endereco VARCHAR(50) NOT NULL,
	sexo CHAR(1),
	datanascimento DATE NOT NULL,
	CHECK (sexo IN ('m','f','o')),
	CONSTRAINT cpf_valido CHECK (cpf ~ ('([0-9]{11})'))
);
CREATE TABLE telefone(
	telefone VARCHAR(9) NOT NULL,
	cod_cliente NUMERIC,
	FOREIGN KEY (cod_cliente) REFERENCES cliente (codigo),
	CONSTRAINT cliente_telefone PRIMARY KEY (telefone,cod_cliente)
);
CREATE TABLE funcionario(
	codigo NUMERIC PRIMARY KEY,
	matricula CHAR(6) NOT NULL UNIQUE,
	nome VARCHAR(45) NOT NULL
);
CREATE TABLE carro(
	codigo NUMERIC PRIMARY KEY,
	modelo VARCHAR(45) NOT NULL,
	marca VARCHAR(45),
	placa CHAR(8) UNIQUE,
	CONSTRAINT placa_valida CHECK (placa ~* '([A-Z]{3})-([0-9]{4})')
);
CREATE TABLE aluguel(
	cod_cliente NUMERIC,
	cod_funcionario NUMERIC,
	cod_carro NUMERIC,
	DataLocacao TIMESTAMP,
	DataEntrega TIMESTAMP,
	CHECK (DataEntrega>DataLocacao),
	FOREIGN KEY (cod_cliente) REFERENCES cliente (codigo),
	FOREIGN KEY (cod_funcionario) REFERENCES funcionario (codigo),
	FOREIGN KEY (cod_carro) REFERENCES carro (codigo),
	CONSTRAINT cod_aluguel PRIMARY KEY (cod_cliente, cod_funcionario, cod_carro, DataLocacao)
);